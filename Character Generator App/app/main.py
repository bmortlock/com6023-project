import os
import random
from flask import Flask, render_template, request
app = Flask(__name__)

@app.route('/')
def home():
   return render_template("home.html")
   
@app.route('/Charsheet', methods=["POST"] )
def process():
	fname = request.form['fname']
	lname = request.form['lname']
	age = request.form['age']
	eyecolour = request.form['eyecolour']
	glasses = request.form['glasses']
	traits = request.form['traits']
	haircolour = request.form['haircolour']
	gender = request.form['gender']
	likes = request.form['likes']
	hates = request.form['hates']
	job = request.form['job']
	height = request.form['height']
	weight = request.form['weight']
	food = request.form['food']
	
	
	imgsblndf = os.listdir('static/img/blond/female')
	imgsblndf = ['img/blond/female/' + file for file in imgsblndf]
	imgrandblndf = random.sample(imgsblndf,k=1)
	
	imgsblndm = os.listdir('static/img/blond/male')
	imgsblndm = ['img/blond/male/' + file for file in imgsblndm]
	imgrandblndm = random.sample(imgsblndm,k=1)
	
	imgsblkf = os.listdir('static/img/black/female')
	imgsblkf = ['img/black/female/' + file for file in imgsblkf]
	imgrandblkf = random.sample(imgsblkf,k=1)
	
	imgsblkm = os.listdir('static/img/black/male')
	imgsblkm = ['img/black/male/' + file for file in imgsblkm]
	imgrandblkm = random.sample(imgsblkm,k=1)
	
	imgsbrwnf = os.listdir('static/img/brown/female')
	imgsbrwnf = ['img/brown/female/' + file for file in imgsbrwnf]
	imgrandbrwnf = random.sample(imgsbrwnf,k=1)
	
	imgsbrwnm = os.listdir('static/img/brown/male')
	imgsbrwnm = ['img/brown/male/' + file for file in imgsbrwnm]
	imgrandbrwnm = random.sample(imgsbrwnm,k=1)
	
	imgsredf = os.listdir('static/img/red/female')
	imgsredf = ['img/red/female/' + file for file in imgsredf]
	imgrandredf = random.sample(imgsredf,k=1)
	
	imgsredm = os.listdir('static/img/red/male')
	imgsredm = ['img/red/male/' + file for file in imgsredm]
	imgrandredm = random.sample(imgsredm,k=1)
	
	imgsbluef = os.listdir('static/img/blue/female')
	imgsbluef = ['img/blue/female/' + file for file in imgsbluef]
	imgrandbluef = random.sample(imgsbluef,k=1)
	
	imgsbluem = os.listdir('static/img/blue/male')
	imgsbluem = ['img/blue/male/' + file for file in imgsbluem]
	imgrandbluem = random.sample(imgsbluem,k=1)
	
	imgsgrnf = os.listdir('static/img/green/female')
	imgsgrnf = ['img/green/female/' + file for file in imgsgrnf]
	imgrandgrnf = random.sample(imgsgrnf,k=1)
	
	imgsgrnm = os.listdir('static/img/green/male')
	imgsgrnm = ['img/green/male/' + file for file in imgsgrnm]
	imgrandgrnm = random.sample(imgsgrnm,k=1)
	
	imgswhtf = os.listdir('static/img/white/female')
	imgswhtf = ['img/white/female/' + file for file in imgswhtf]
	imgrandwhtf = random.sample(imgswhtf,k=1)
	
	imgswhtm = os.listdir('static/img/white/male')
	imgswhtm = ['img/white/male/' + file for file in imgswhtm]
	imgrandwhtm = random.sample(imgswhtm,k=1)
	
	return render_template("CharSheet.html", lname=lname, fname=fname, age=age, gender=gender, haircolour=haircolour, eyecolour=eyecolour, glasses=glasses, traits=traits, likes=likes, hates=hates, job=job, height=height, weight=weight, food=food, imgrandblndf=imgrandblndf, imgrandblndm=imgrandblndm, imgrandblkf=imgrandblkf, imgrandblkm=imgrandblkm, imgrandbrwnm = imgrandbrwnm, imgrandbrwnf = imgrandbrwnf, imgrandredm = imgrandredm, imgrandredf = imgrandredf, imgrandbluef=imgrandbluef, imgrandbluem=imgrandbluem, imgrandgrnf=imgrandgrnf, imgrandgrnm=imgrandgrnm, imgrandwhtf=imgrandwhtf, imgrandwhtm=imgrandwhtm )





if __name__ == '__main__':
  app.run()
